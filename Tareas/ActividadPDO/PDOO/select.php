<html>
    <head>
        <title>Consulta de datos</title>
    </head>
    <body>
    <h3>Consulta de datos</h3>
    <?php
    try {
        require_once('./conn.php');
        echo "<h3> Consulta de datos PDO::FETCH_ASSOC </h3>";
        // consulta de datos
        // FETCH_ASSOC
        $stmt = $dbh->prepare("SELECT * FROM alumnos");
        // Especificamos el fetch mode antes de llamar a fetch()
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        // Ejecutamos
        $stmt->execute();
        // Mostramos los resultados
        while ($row = $stmt->fetch()) {
            echo "numcta: {$row["al_numcta"]} <br>";
			echo "nombre: {$row["al_nombre"]} <br>";
			echo "apellido1: {$row["al_apellido1"]} <br>";
			echo "apellido2: {$row["al_apellido2"]} <br>";
			echo "contraseña: {$row["al_contraseña"]} <br>";
			echo "genero: {$row["al_genero"]} <br>";
		//	echo "fechaNac: {$row["al_fechaNac"]} <br>";
         
	     // echo "Director: {$row["cam_director"]} <br><br>";
        }
        
        echo "<hr/><h3>Consulta de datos PDO::FETCH_OBJ </h3>";
        // FETCH_OBJ
        $stmt = $dbh->prepare("SELECT * FROM alumnos");
        $stmt->execute();
        // Otra forma de especificar el fetch mode
        while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
            echo "id: {$row->alumno_id} <br>";
			echo "numcta: {$row->al_numcta} <br>";
			echo "nombre: {$row->al_nombre} <br>";
			echo "apellido1: {$row->al_apellido1} <br>";
			echo "apellido2: {$row->al_apellido2} <br>";
			echo "contraseña: {$row->al_contraseña} <br>";
			echo "genero: {$row->al_genero} <br>";
			echo "fechaNac: {$row->al_fechaNac} <br>";
           // echo "Nombre: {$row->rec_nombre} <br>";
          //  echo "Director: {$row->cam_director} <br><br>";
        }
    } catch (Exception $e) {
        // Cualquier error lo imprimimos
        echo $e->getMessage();
    } finally {
        // Cerramos la conexion a la base
        $dbh = null;
    }
    ?>
    <ul>
        <li><a href='index.php'>Index</a></li>
        <li><a href='insert.php'>Insertar datos</a></li>
        <li><a href='transaccion.php'>Transacciones</a></li>
    </ul>
    </body>
</html>
