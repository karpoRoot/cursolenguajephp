<html>
    <head>
        <title>Transacciones</title>
    </head>
    <body>
        <h3>Transacciones con PDO</h3>
        <p>Transacci&oacute;n exitosa (commit)</p>
        <?php
        try {
            require_once('./conn.php');
            $dbh->beginTransaction();
            $dbh->query('INSERT INTO alumnos (id) VALUES ("100"),("101"),("102"),("103")');
            echo "Se insertaron los alumnos 100, 101, 102, 103 <br />";
            $dbh->query("DELETE FROM alumnos WHERE id > 100");
            echo "Se borraron los alumnos mayores a 100 <br />";
            $dbh->query("UPDATE alumnos set al_numcta = CONCAT('alumnos ', id) WHERE id >= 100");
            echo "Se actualizo el nombre del alumno con id mayor igual a 100 <br />";
            $dbh->commit();
            echo "Se ha realizado las operaciones en alumno";
            ?>
            <hr /><p>Transacci&oacute;n que no guarda (rollback)</p>
            <?php
            // Tambien se puede hacer transacciones con prepare, con bindValue o bindParam y execute
            $dbh->beginTransaction();
            // Insertamos datos
            $sql = "INSERT INTO alumnos(id, al_numcta, al_apellido1, al_apellido2, al_contraseña, al_fechaNac) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            // Datos 1: Parámetros posicion
            $stmt = $dbh->prepare($sql);
            $id = 41;
            $al_numcta = "10000001";
            $al_nombre = "pedro Stark ";
            $stmt->bindParam(1, $id);
            $stmt->bindParam(2, $al_numcta);
            $stmt->bindParam(3, $al_nombre);
			$stmt->bindParam(3, $al_apellido1);
			$stmt->bindParam(3, $al_apellido2);
			$stmt->bindParam(3, $al_contraseña);
			$stmt->bindParam(3, $al_genero);
			$stmt->bindParam(3, $al_fechaNac);
            echo ($stmt->execute()) ? "Se agrego a $nombre" : '';
            echo "<br />";
        
            // Datos 2: Parametros nombrados
            $sql = "INSERT INTO alumnos(id, al_numcta, al_apellido1, al_apellido2, al_contraseña, al_fechaNac) VALUES (:id, :al_nombre, :al_apellido1, :al_apellido2, :al_contraseña, :al_genero, :al_fechaNac )";
            $stmt = $dbh->prepare($sql);
            $id = 42;
            $al_numcta = "00000000";
            $al_nombre = "sergio ";
            $stmt->bindParam(":id", $id);
            $stmt->bindParam(":al_numcta", $al_numcta);
            $stmt->bindParam(":al_nombre", $al_nombre);
			$stmt->bindParam(":al_apellido1", $al_apellido1);
			$stmt->bindParam(":al_apellido2", $al_apellido2);
			$stmt->bindParam(":al_contraseña", $al_contraseña);
			$stmt->bindParam(":al_genero", $al_genero);
			$stmt->bindParam(":al_fechaNac", $al_fechaNac);
            echo ($stmt->execute()) ? "Se agrego a $al_nombre" : '';
            echo "<br />";
        
            // Datos 3: Modo Lazy
            $stmt = $dbh->prepare($sql);
            echo($stmt->execute([':id'=> '43', ':al_numcta'=>'00000000', ':al_nombre'=>'sergio', ':al_apellido1'=>'Stark', ':al_apellido2'=>'sergio', ':al_contraseña'=>'sergio', ':al_genero'=>'sergio', ':al_fechaNac'=>'sergio'])) ? 'Se agrego a sergio' : '';
            echo "<br />";

            // Actualizacion
            $dbh->query("UPDATE alumnos set id = 1"); // actualizaqr todos los id a 1
            echo "Se actualizo la tabla alumnos id = 1 <br />";
            // Borramos toda la tabla
            $dbh->query("DELETE FROM alumnos"); // descomentar para borrar toda la tabla
            echo "Se borro toda la informacion de la tabla alumnos <br />";
            // Hacemos que no guarde la transaccion
            $dbh->rollback();
            echo "Se hizo el rollback <br />";
        } catch (Exception $e) {
            // Cualquier error lo imprimimos
            $dbh->rollback();
            echo $e->getMessage();
        } finally {
            // Cerramos la conexion a la base
            $dbh = null;
        }
        ?>
        <ul>
            <li><a href='index.php'>Index</a></li>
            <li><a href='insert.php'>Insertar datos</a></li>
            <li><a href='select.php'>Consultar los datos</a></li>
        </ul>
    </body>
</html>
