<html>
<head>
    <title>Inserci&oacute;n de datos</title>
</head>
<body>
<h3>Insertar datos</h3>
<?php
class Alum
{
    public $id;
    public $al_numcta;
    public $al_nombre;
	public $al_apellido1;
	public $al_apellido2;
	public $al_contraseña;
	public $al_genero;
	public $al_fechaNac;
    public function __construct($id, $al_numcta, $al_nombre,$al_apellido1, $al_apellido2, $al_contraseña, $al_genero, $al_fechaNac)
    {
        $this->id = $id;
        $this->al_numcta = $nombre;
        $this->al_nombre = $al_nombre;
		$this->al_apellido1 = $al_apellido1;
		$this->al_apellido2 = $al_apellido2;
		$this->al_contraseña = $al_contraseña;
		$this->al_genero = $al_genero;
		$this->al_fechaNac = $al_fechaNac;
    }
}
try {
    // Preparamos la conexion a la base de datos
    require_once('./conn.php');
    // Insertamos datos
    $sql = "insert into alumnos(al_numcta,al_nombre,al_apellido1,al_apellido2,al_contraseña, al_genero, al_fechaNac)values(:al_numcta,:al_nombre,:al_apellido1,:al_apellido2,:al_contraseña,:al_genero,:al_fechaNac)";
    // Datos 1: Parámetros posicion
    $stmt = $dbh->prepare($sql);
    $id = 1;
	$al_numcta = "00598125";
	$al_nombre = "Julio ";
    $stmt->bindParam(1, $id);
    $stmt->bindParam(2, $al_numcta);
    $stmt->bindParam(3, $al_nombre);
	$stmt->bindParam(4, $al_apellido1);
	$stmt->bindParam(5, $al_apellido2);
	$stmt->bindParam(6, $al_contraseña);
	$stmt->bindParam(7, $al_fechaNac);
	
    echo ($stmt->execute()) ? "Se agrego a $nombre" : '';
    echo "<br />";

    // Datos 2: Parametros nombrados
    $sql = "INSERT INTO alum(rector_id, rec_nombre, cam_director) VALUES (:id, :nombre, :director)";
    $stmt = $dbh->prepare($sql);
    $id = 2;
	$al_numcta = "00000569";
	$al_nombre = "Alberto ";
    $stmt->bindParam(":id", $id);
	$stmt->bindParam(":al_numcta", $al_numcta);
	$stmt->bindParam(":al_nombre", $al_nombre);
	$stmt->bindParam(":al_apellido1", $al_apellido1);
	$stmt->bindParam(":al_apellido2", $al_apellido2);
	$stmt->bindParam(":al_contraseña", $al_contraseña);
	$stmt->bindParam(":al_genero", $al_genero);
	$stmt->bindParam(":al_fechaNac", $al_fechaNac);
    echo ($stmt->execute()) ? "Se agrego a $nombre" : '';
    echo "<br />";

    // Datos 3: Modo Lazy
    $stmt = $dbh->prepare($sql);
	echo($stmt->execute([':id'=> '43', ':al_numcta'=>'00000000', ':al_nombre'=>'sergio', ':al_apellido1'=>'Stark', ':al_apellido2'=>'sergio', ':al_contraseña'=>'sergio', ':al_genero'=>'sergio', ':al_fechaNac'=>'sergio'])) ? 'Se agrego a sergio' : '';
	echo "<br />";

    //Datos 4: modo clase
    $alum = new Alum("3", "000000022", "Agustin");
    $stmt = $dbh->prepare($sql);
    echo($stmt->execute((array) $alum))? "Se agrego a {$alum->nombre}" : '';
    echo "<br />";
} catch (Exception $e) {
    // Cualquier error lo imprimimos
    echo $e->getMessage();
} finally {
    // Cerramos la conexion a la base
    $dbh = null;
}
?>
<ul>
    <li><a href='index.php'>Index</a></li>
    <li><a href='select.php'>Consultar los datos</a></li>
    <li><a href='transaccion.php'>Transacciones</a></li>
</ul>
</body>
</html>
