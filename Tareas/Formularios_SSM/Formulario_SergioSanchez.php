<html>
<head>
    <title>Formularios</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    <link rel="stylesheet"
          href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
	<style>
	 form{
		 margin: 0 auto;
		 width: 400px
	 }
	
	</style>
</head>
    <body>
	<div class="container">
		<div class="columns">
			
			<form action="procesar_formulario.php?accion=get&texto=textoenget" method="POST">
			    <label class="form-label" for="input-text">Número de Cuenta</label>
				<input name="texto" class="form-input " type="number" id="input-numero de cuenta" placeholder="Número de Cuenta">



				<label class="form-label" for="input-text">Nombre:</label>
				<input name="texto" class="form-input " type="text" id="input-nombre" placeholder="Nombre" size="40">
				
				<label class="form-label" for="input-text">Primer Apellido:</label>
				<input name="texto" class="form-input " type="text" id="input-Primer Apellido" placeholder="Primer Apellido">
				
				<label class="form-label" for="input-text">Segundo Apellido:</label>
				<input name="texto" class="form-input " type="text" id="input-Segundo Apellido" placeholder="Segundo Apellido">

				<label class="form-label" for="input-password">Contraseña</label>
				<input name="password" class="form-input" type="password" id="input-password"
					   placeholder="Contraseña">
					   

				<label class="form-label">Genero</label>
				<label class="form-radio">
					<input type="radio" name="sexo" value="H" checked>
					<i class="form-icon"></i> H
				</label>
				<label class="form-radio">
					<input type="radio" name="sexo" value="M">
					<i class="form-icon"></i> M
				</label>
                <label class="form-radio">
					<input type="radio" name="sexo" value="M">
					<i class="form-icon"></i> Otro
				</label>

				<label class="form-label" for="input-date">Fecha de Nacimiento</label>
				<input name="date" class="form-input " type="date" id="input-date"
					   placeholder="fecha de nacimiento">



				<input type='submit' class="btn" value="Enviar"/>
				<input type='reset' class="btn btn-primary" value="limpiar"/>
			</form>
		</div>
	</div>
    </body>
</html>