

<html>

  <head>
     <title>ExprecionesRegulares</title>
  </head>
  
  <body>
       <?php 
	   
	     // Crear una expresión regular para detectar que la longitud máxima de una 
		 //palabra en un texto sea de 30 caracteres *Ejemplo de diapositiva*
	     $check="012345678901234567890123456789";
		 $result=preg_match('/^((.){1,30}(\s+(.){1, 30})*)$/i', $check);
		 echo $result;
		 echo "<br />";
		 echo "<br />";
		 
		 // Ejemplo simple de exprecion regular:
		 $expre="martes";
		 $expresion = preg_match("/martes/", $expre);
		 echo $expresion;
		 echo "<br />";
		 echo "<br />";
///////////////////////////////////////////////////////////////////////////////////////
//////                     ACTIVIDAD_TAREA: Expresiones Regulares con PHP:        ////               
//////////////////////////////////////////////////////////////////////////////////////




		 //Realizar una expresión regular que detecte emails correctos.
		 $correo="lopez@gmail.com";
		 $result1=preg_match ("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $correo);
		 echo $result1;

    	 echo "<br />";
		 echo "<br />";



		 
		 //Realizar una expresion regular que detecte Curps Correctos 
		 //ABCD123456EFGHIJ78.
		 
		 $curp="ABCD123456EFGHIJ78";
		 $expresion = preg_match( "/^([A-Z]||[a-z]{4})(\d{6})([A-Z]||[a-z]{6})(\d{2})$/i", $curp);
		 echo $expresion;
		 
		 echo "<br />";
		 echo "<br />";

		 
		 //Realizar una expresion regular que detecte palabras de longitud mayor a 50 
		 //formadas solo por letras.
		 
		 $palabra="RealizarunaexpresionregularquedetectepalabrasdDElongitudmayoraCINCUENTAhkjhjkhjkhjkhjkh";
		 $deteccion=preg_match('/^([A-Z]||[a-z]{51,})$/i', $palabra);
		 echo $deteccion;
		 echo "<br />";
		 echo "<br />";
		 
// función para escapar los símbolos especiales.

         $simbolo = '$10  pj/20';
         $simbolo = preg_quote($simbolo, '/');
         echo $simbolo;
		 echo "<br />";
		 echo "<br />";

 //Crear una expresion regular para detectar números decimales.
		 $numero="82.5";
		 $validacion=preg_match('/^[0-9]+([.][0-9]+)?$/', $numero);
		 echo $validacion;
		 echo "<br />";
		 echo "<br />";




		 
		 
	   ?>
  </body>
  
</html>











